# Python template

**Project template for pyhton projects**


### Install dependencies for app and tests

In project root:
```
python3 -m venv venv
. venv/bin/activate
pip install -e .
pip install -e .[test]
```


## Run
In project root:
```
. venv/bin/activate
python -m pytest
```
