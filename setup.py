from setuptools import find_packages, setup

setup(
    name="python template",
    version="0.1",
    description="Template for python projects.",
    author="",
    packages=find_packages(where="src"),
    package_dir={"": "src"},

    entry_points={
    },

    install_requires=[
        "toml",
    ],

    extras_require={
        "test": [
            "wheel",
            "pytest",
            "requests",
        ],
    },

)

